# README #

My todo list app, built atop of [Meteor](http://meteor.com).

![todo list](https://media.giphy.com/media/l46CkPpXydguuDUfm/giphy.gif)
![todo list](http://i.giphy.com/l46C4iBmOT407bWiA.gif)

### How do I get set up? ###

```
#!bash

npm install
npm start

```