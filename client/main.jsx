import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import '../imports/startup/accounts-config.js';
import App from '../imports/ui/App.jsx';
//import Utils from '../imports/utils/Utils.jsx';

const title = 'Meteoric Todo List';
const subTitle = 'To-do list created using Meteor + React.';

Meteor.startup(() => {
    render(<App
        title={title}
        subTitle={subTitle}
    />, document.getElementById('render-target'));
});