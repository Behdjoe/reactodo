import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import { Tasks } from '../api/tasks.js';

import Task from './Task.jsx';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';

// App component - represents the whole app
class App extends Component {


    constructor(props) {
        super(props);

        this.state = {
            hideCompleted: false,
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        // Find the text field via the React ref
        const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
        if (text == ''){return;}

        Tasks.insert({
            text,
            createdAt: new Date(), // current time
            owner: Meteor.userId(),           // _id of logged in user
            username: Meteor.user().username,  // username of logged in user
            checked: false,  // username of logged in user
        });

        // Clear form
        ReactDOM.findDOMNode(this.refs.textInput).value = '';
    }

    toggleHideCompleted() {
        this.setState({
            hideCompleted: !this.state.hideCompleted,
        });

        const hideCompleted = this.state.hideCompleted ? 'hidden' : '';
    }

    renderTasks() {
        let filteredTasks = this.props.tasks;
        filteredTasks = filteredTasks.filter(task => !task.checked);
        // if (this.state.hideCompleted) {
        //     filteredTasks = filteredTasks.filter(task => !task.checked);
        // }
        return filteredTasks.map((task) => (
            <Task key={task._id} task={task}  />
        ));
    }
    renderDoneTasks() {
        let doneTasks = this.props.tasks;
        doneTasks = doneTasks.filter(task => task.checked);
        // if (!this.state.hideCompleted) {
        //     filteredTasks = filteredTasks.filter(task => task.checked);
        // }
        return doneTasks.map((task) => (
            <Task key={task._id} task={task}/>
        ));
    }

    render() {
        return (
            <div className="container">
                <header>
                    <h1>{this.props.title} ({this.props.incompleteCount})</h1>
                    <h4>{this.props.subTitle}</h4>
                    <br />
                    <AccountsUIWrapper />
                </header>
                { this.props.currentUser ?
                    <form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
                        <input
                            autofocus
                            type="text"
                            ref="textInput"
                            placeholder="Type to add new tasks"
                        />
                    </form> : ''
                }
                <ul className="taskList">{this.renderTasks()}</ul>
                <ul className="doneTasks ">
                    {this.props.completedCount ?
                        <h3>Done ({this.props.completedCount})</h3> : ''
                    }
                    {this.renderDoneTasks()}
                </ul>
            </div>
        );
    }
}

App.propTypes = {
    tasks: PropTypes.array.isRequired,
    incompleteCount: PropTypes.number.isRequired,
    currentUser: PropTypes.object,
};

export default createContainer(() => {
    return {
        tasks: Tasks.find({}, { sort: { createdAt: -1} }).fetch(),
        incompleteCount: Tasks.find({ checked: { $ne: true } }).count(),
        completedCount: Tasks.find({ checked: { $ne: false } }).count(),
        currentUser: Meteor.user(),
    };
}, App);