import { Meteor } from 'meteor/meteor';
import React, { Component, PropTypes } from 'react';

import { Tasks } from '../api/tasks.js';

// Task component - represents a single todo item
export default class Task extends Component {
    constructor(props) {
        super(props);

        this.state = {
            taskSaved: false,
        };
    }
    toggleChecked() {
        if(this.props.task.owner != Meteor.userId()){
            return;
        }
        // Set the checked property to the opposite of its current value
        Tasks.update(this.props.task._id, {
            $set: { checked: !this.props.task.checked },
        });
    }
    toggleStarred() {
        if(this.props.task.owner != Meteor.userId()){
            return;
        }
        Tasks.update(this.props.task._id, {
            $set: { starred: !this.props.task.starred },
        });
    }
    editTask(event) {
        if (this.props.task.owner != Meteor.userId()){
            return;
        }
        const oldText = this.props.task.text;
        const myText = event.target.value.trim();
        if (oldText == myText || myText == ''){
            return;
        }
        Tasks.update(this.props.task._id, {
            $set: { text: myText},
        });

        this.setState({
            taskSaved: true,
        });

        const saved = this.state.taskSaved ? 'i' : '';
        //console.log(this.props.task._id + ' was edited: ' + myText);
    }
    deleteThisTask() {
        if(this.props.task.owner != Meteor.userId()){
            return;
        }
        Tasks.remove(this.props.task._id);
    }

    render() {
        console.warn(this.props.key);
        // Give tasks a different className when they are checked off,
        // so that we can style them nicely in CSS
        const checkedClass = this.props.task.checked ? 'done animated bounceInUp' : 'animated flipInX';
        const starredClass = this.props.task.starred ? 'starred' : '';
        const notStarred = (this.props.task.starred) ? "removestar" : '';
        const readOnlyClass = (this.props.task.owner == Meteor.userId()) ? "myTask" : 'readonly';

        return (
            <li className={'task ' + checkedClass + ' ' + starredClass}>
                <label className="label" title="Mark as done">
                    <input
                        readOnly
                        type="checkbox"
                        className="checkbox"
                        checked={this.props.task.checked}
                        onClick={this.toggleChecked.bind(this)}
                    />
                    {(this.props.task.owner != Meteor.userId()) ?
                        <span>{this.props.task.text}</span> :
                        <input
                            id={this.props.task._id}
                            className={"editTask " + readOnlyClass}
                            type="text"
                            defaultValue={this.props.task.text}
                            onBlur={this.editTask.bind(this)}
                        /> }
                </label>
                {(this.state.taskSaved) ?
                    <span>{saved}</span> : ''
                }
                <label className={'unstarred ' + notStarred} title="Mark as starred">
                    <input
                        type="checkbox"
                        hidden
                        readOnly
                        checked={this.props.task.starred}
                        onClick={this.toggleStarred.bind(this)}
                    />
                </label>

                {(this.props.task.owner == Meteor.userId()) ?
                    <button className="delete close-button" title="Delete" onClick={this.deleteThisTask.bind(this)}>
                        &times;
                    </button> : ''
                }
            </li>
        );
    }
}
Task.myProps = {
   savedMessage: 'i'
}
Task.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    task: PropTypes.object.isRequired,
};